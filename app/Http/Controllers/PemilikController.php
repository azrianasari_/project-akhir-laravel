<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// hubungkan model pemilik dengan controller pemilik
use App\Pemilik;
class PemilikController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //ambil semua data dari tabel pemilik di database
        $data = Pemilik::all();

        //tampilkan dan arahkan ke halaman index
        return view('pemilik.index', compact ('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //arahkan ke halaman create
        return view('pemilik.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //cek data telepon, required = tidak boleh kosong, ,max:12 = karakter yang diinputkan tidak boleh lebih dari 12
        $request->validate([
            'telp' => 'required|max:12',
        ]);
    
        //save data ke database, 'Pemilik' adalah nama model
        Pemilik::create([
            'nama' => $request->nama,
            'alamat' => $request->alamat,
            'kelurahan' => $request->kelurahan,
            'kecamatan' => $request->kecamatan,
            'kab_kota' => $request->kab_kota,
            'kode' => $request->kode,
            'kode_pos' => $request->kode_pos,
            'telp' => $request->telp,
        ]);

        //arahkan ke halaman pemilik dan tampilkan pesan/status
        return redirect('/pemilik')->with('status', 'Data Berhasil Ditambah');
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //tampilkan data berdasarkan $id data
        $data = Pemilik::findOrFail($id);

        //arahkan dan tampilkan data pada halaman edit
        return view('pemilik.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //tampilkan data berdasarkan $id data
        $data = Pemilik::findOrFail($id);

        //arahkan dan tampilkan data pada halaman edit
        return view('pemilik.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //cek data telepon yang diinputkan, required = tidak boleh kosong, ,max:12 = karakter yang diinputkan tidak boleh lebih dari 12
        $request->validate([
            'telp' => 'required|max:12',
        ]);
            
        //update data berdasarkan id
        Pemilik::where('id', $id)
                ->update([
                    'nama' => $request->nama,
                    'alamat' => $request->alamat,
                    'kelurahan' => $request->kelurahan,
                    'kecamatan' => $request->kecamatan,
                    'kab_kota' => $request->kab_kota,
                    'kode' => $request->kode,
                    'kode_pos' => $request->kode_pos,
                    'telp' => $request->telp,
                    ]);
        
        //arahkan ke halaman pemilik dan tampilkan pesan/status
        return redirect('/pemilik')->with('status', 'Data Berhasil Diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //hapus data berdasarkan $id
        Pemilik::destroy($id);

        //arahkan ke halaman pemilik dan tampilkan pesan/status
        return redirect('/pemilik')->with('status', 'Data Berhasil Dihapus');
    }
}
