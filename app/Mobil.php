<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mobil extends Model
{
     //nama tabel
     protected $table = "mobils";

     //nama kolom
     protected $fillable = ['kode','tahun','warna','no_plat','no_mesin','no_rangka','status_mobil','merk','tipe','foto','pemilik_id'];

     //relasi ke tabel pemilik
     public function pemilik()
     {
     	return $this->belongsTo('App\Pemilik');
     }

     public function getStatusMobilFormatted() {
          if($this->getAttribute('status_mobil') == 0) {
              return "Tersedia";
          }else {
              return "Tidak Tersedia";
          }
     }
}
