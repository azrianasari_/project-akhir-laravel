@extends('layouts.app')

{{-- isi @yield('content') --}}
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Detail Data</div>

                <div class="card-body">
                 

                    <div class="card-body">
                        <form >
                            
                            <div class="form-group row">
                                <label for="kode" class="col-md-3 offset-md-1 col-form-label text-md-left">Kode </label>
                                
                                <label for="kode" class="col-md-3 offset-md-1 col-form-label text-md-left">: {{ $data->kode }} </label>
                            </div>

                            <div class="form-group row">
                                <label for="tahun" class="col-md-3 offset-md-1 col-form-label text-md-left">Tahun </label>
                                
                                <label for="tahun" class="col-md-3 offset-md-1 col-form-label text-md-left">: {{ $data->tahun }} </label>
                            </div>

                            <div class="form-group row">
                                <label for="warna" class="col-md-3 offset-md-1 col-form-label text-md-left">Warna </label>
                                
                                <label for="warna" class="col-md-3 offset-md-1 col-form-label text-md-left">: {{ $data->warna }} </label>
                            </div>

                            <div class="form-group row">
                                <label for="no_plat" class="col-md-3 offset-md-1 col-form-label text-md-left">No Plat </label>
                                
                                <label for="no_plat" class="col-md-3 offset-md-1 col-form-label text-md-left">: {{ $data->no_plat }} </label>
                            </div>

                            <div class="form-group row">
                                <label for="no_mesin" class="col-md-3 offset-md-1 col-form-label text-md-left">No Mesin </label>
                                
                                <label for="no_mesin" class="col-md-3 offset-md-1 col-form-label text-md-left">: {{ $data->no_mesin }} </label>
                            </div>
                            <div class="form-group row">
                                <label for="no_rangka" class="col-md-3 offset-md-1 col-form-label text-md-left">No Rangka </label>
                                
                                <label for="no_rangka" class="col-md-3 offset-md-1 col-form-label text-md-left">: {{ $data->no_rangka }} </label>
                            </div>
                            <div class="form-group row">
                                <label for="status_mobil" class="col-md-3 offset-md-1 col-form-label text-md-left">Status Mobil </label>
                                
                                <label for="status_mobil" class="col-md-3 offset-md-1 col-form-label text-md-left">: {{ $data->status_mobil }} </label>
                            </div>
                            <div class="form-group row">
                                <label for="merk" class="col-md-3 offset-md-1 col-form-label text-md-left">Merk </label>
                                
                                <label for="merk" class="col-md-3 offset-md-1 col-form-label text-md-left">: {{ $data->merk }} </label>
                            </div>
                            <div class="form-group row">
                                <label for="tipe" class="col-md-3 offset-md-1 col-form-label text-md-left">Tipe </label>
                                
                                <label for="tipe" class="col-md-3 offset-md-1 col-form-label text-md-left">: {{ $data->tipe }} </label>
                            </div>
                            <div class="form-group row">
                                <label for="pemilik_id" class="col-md-3 offset-md-1 col-form-label text-md-left">Pemilik </label>
                                
                                <label for="pemilik_id" class="col-md-3 offset-md-1 col-form-label text-md-left">: {{ $data->pemilik->nama }} </label>
                            </div>
                            <div class="form-group row">
                                <label for="foto" class="col-md-3 offset-md-1 col-form-label text-md-left">Foto </label>
                                
                                <img src="{{asset('storage/images/'.$data->foto)}}" alt="" >
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection