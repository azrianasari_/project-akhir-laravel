@extends('layouts.app')

{{-- isi @yield('content') --}}
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">List Data</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="row text-right">
                        <div class="col-lg-12">
                            <a href="/pemilik/create" class="btn btn-primary btn-sm">
                              Tambah Data
                            </a>
                        </div>
                    </div>

                    <div class="row ml-1">   
                        <table border=1 cellpadding="10">
                            <thead style="text-align:center">
                                <th>No</th>
                                <th>Kode</th>
                                <th>Nama</th>
                                <th>Alamat</th>
                                <th>Telepon</th>
                                <th>Aksi</th>
                            </thead>
                            <tbody>
                            @foreach ($data as $dt)
                                <tr>
                                    <td>{{ $loop->iteration }} </td>   
                                    <td>{{ $dt->kode }} </td>   
                                    <td>{{ $dt->nama }} </td>   
                                    <td>{{ $dt->alamat }} </td>   
                                    <td>{{ $dt->telp }} </td>   
                                  
                                    <td>
                                      <a href={{"pemilik/$dt->id/edit"}} class="btn btn-warning btn-sm">Edit</a> 
                                      <form action={{"/pemilik/$dt->id"}} method="POST" class="d-inline">
                                        @method('delete')
                                        @csrf
                                          <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Yakin?')">Hapus</button>
                                      </form>
                                      <a href={{"pemilik/$dt->id"}} class="btn btn-primary btn-sm" >Detail</a>
                                    </td>  
                                </tr> 
                            @endforeach  
                            </tbody>
                        </table>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection