@extends('layouts.app')

{{-- isi @yield('content') --}}
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Detail Data</div>

                <div class="card-body">
                 

                    <div class="card-body">
                        <form >
                            
                            <div class="form-group row">
                                <label for="kode" class="col-md-3 offset-md-1 col-form-label text-md-left">Kode </label>
                                
                                <label for="kode" class="col-md-3 offset-md-1 col-form-label text-md-left">: {{ $data->kode }} </label>
                            </div>

                            <div class="form-group row">
                                <label for="nama" class="col-md-3 offset-md-1 col-form-label text-md-left">Nama </label>
                                
                                <label for="nama" class="col-md-3 offset-md-1 col-form-label text-md-left">: {{ $data->nama }} </label>
                            </div>

                            <div class="form-group row">
                                <label for="alamat" class="col-md-3 offset-md-1 col-form-label text-md-left">Alamat </label>
                                
                                <label for="alamat" class="col-md-3 offset-md-1 col-form-label text-md-left">: {{ $data->alamat }} </label>
                            </div>

                            <div class="form-group row">
                                <label for="kelurahan" class="col-md-3 offset-md-1 col-form-label text-md-left">Kelurahan </label>
                                
                                <label for="kelurahan" class="col-md-3 offset-md-1 col-form-label text-md-left">: {{ $data->kelurahan }} </label>
                            </div>

                            <div class="form-group row">
                                <label for="kecamatan" class="col-md-3 offset-md-1 col-form-label text-md-left">Kecamatan </label>
                                
                                <label for="kecamatan" class="col-md-3 offset-md-1 col-form-label text-md-left">: {{ $data->kecamatan }} </label>
                            </div>
                            <div class="form-group row">
                                <label for="kab_kota" class="col-md-3 offset-md-1 col-form-label text-md-left">Kabupaten/Kota </label>
                                
                                <label for="kab_kota" class="col-md-3 offset-md-1 col-form-label text-md-left">: {{ $data->kab_kota }} </label>
                            </div>
                            <div class="form-group row">
                                <label for="kode_pos" class="col-md-3 offset-md-1 col-form-label text-md-left">Kode Pos </label>
                                
                                <label for="kode_pos" class="col-md-3 offset-md-1 col-form-label text-md-left">: {{ $data->kode_pos }} </label>
                            </div>
                            <div class="form-group row">
                                <label for="telp" class="col-md-3 offset-md-1 col-form-label text-md-left">Telepon </label>
                                
                                <label for="telp" class="col-md-3 offset-md-1 col-form-label text-md-left">: {{ $data->telp }} </label>
                            </div>
    
                        
                            <div class="form-group row mb-0">
                               
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection